create table group_services (
    id                 serial
        primary key,
    create_at          timestamp    null,
    create_by          bigint       null,
    update_at          timestamp    null,
    update_by          bigint       null,
    delete_at          timestamp    null,
    delete_by          bigint       null,
    group_service_code varchar(50)  not null,
    group_service_name varchar(255) not null,
    status             varchar(100) not null,
    description        varchar(255) null
);

create table services (
    id                    serial
        primary key,
    create_at             timestamp    null,
    create_by             bigint       null,
    update_at             timestamp    null,
    update_by             bigint       null,
    delete_at             timestamp    null,
    delete_by             bigint       null,
    service_code          varchar(50)  not null,
    service_name          varchar(255) not null,
    external_service_code varchar(50)  null,
    trans_type_id         bigint,
    description           varchar(255) null,
    is_finacial           boolean,
    type                  varchar(100) null,
    is_internal           boolean,
    is_intermediary       boolean,
    priority_xpartner_id  integer,
    status                varchar(100) null
);

create table group_service_mapping (
    id               serial
        primary key,
    create_at        timestamp    null,
    create_by        bigint       null,
    update_at        timestamp    null,
    update_by        bigint       null,
    delete_at        timestamp    null,
    delete_by        bigint       null,
    group_service_id bigint       not null,
    service_id       bigint       not null,
    description      varchar(255) null
);

create table partner_info (
    id              serial
        primary key,
    create_at       timestamp    null,
    create_by       bigint       null,
    update_at       timestamp    null,
    update_by       bigint       null,
    delete_at       timestamp    null,
    delete_by       bigint       null,
    role_type_id    integer,
    partner_code    varchar(100) null,
    partner_name    varchar(255) null,
    is_intermediary varchar(255) null,
    status          varchar(100) null
);

create table product_list (
    id            serial
        primary key,
    create_at     timestamp    null,
    create_by     bigint       null,
    update_at     timestamp    null,
    update_by     bigint       null,
    delete_at     timestamp    null,
    delete_by     bigint       null,
    role_type_id  integer,
    partner_id    bigint       not null,
    services_id   bigint       not null,
    product_code  varchar(100) not null,
    product_name  varchar(255) not null,
    amount        decimal      not null,
    currency_code varchar(255) null,
    status        varchar(100) null
);

create table partner_wallets (
    id                        serial
        primary key,
    create_at                 timestamp    null,
    create_by                 bigint       null,
    update_at                 timestamp    null,
    update_by                 bigint       null,
    delete_at                 timestamp    null,
    delete_by                 bigint       null,
    role_type_id              integer      null,
    partner_id                bigint       not null,
    accounting_wallet_type_id integer      null,
    wallet_id                 integer      not null,
    wallet_code               varchar(255) not null,
    type                      boolean,
    currency_code             varchar(255) null,
    is_owner                  boolean
);

create table partner_service (
    id                   serial
        primary key,
    create_at            timestamp    null,
    create_by            bigint       null,
    update_at            timestamp    null,
    update_by            bigint       null,
    delete_at            timestamp    null,
    delete_by            bigint       null,
    type                 varchar(100) null,
    flag_intermediary    boolean,
    priority_partner_id  bigint       null,
    sub_partner_id       bigint       null,
    partner_service_code varchar(100) not null,
    partner_id           bigint       not null,
    service_id           bigint       not null,
    status               varchar(100) null,
    external_service_id  bigint       not null,
    description          varchar(255) not null,
    fsid_name            varchar(100) null
);

create table partner_service_wallet (
    id                 serial
        primary key,
    create_at          timestamp   null,
    create_by          bigint      null,
    update_at          timestamp   null,
    update_by          bigint      null,
    delete_at          timestamp   null,
    delete_by          bigint      null,
    partner_service_id bigint      not null,
    partner_wallet_id  bigint      not null
)