package com.example.partnerservicev2.repository;

import com.example.partnerservicev2.entity.GroupServicesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupServicesRepository extends JpaRepository<GroupServicesEntity, Long> {
}
