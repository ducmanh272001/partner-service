package com.example.partnerservicev2.dto.groupservices;

import lombok.Data;

import java.math.BigInteger;
import java.time.OffsetDateTime;

@Data
public class GetGroupServicesResponse {
    private Long id;
    private OffsetDateTime create_at;
    private BigInteger create_by;
    private OffsetDateTime update_at;
    private BigInteger update_by;
    private OffsetDateTime delete_at;
    private BigInteger delete_by;
    private String group_service_code;
    private String group_service_name;
    private String status;
    private String description;
}
