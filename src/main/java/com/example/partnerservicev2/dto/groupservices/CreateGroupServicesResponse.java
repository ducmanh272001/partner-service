package com.example.partnerservicev2.dto.groupservices;

import lombok.Data;

@Data
public class CreateGroupServicesResponse {
    private Long id;
}
