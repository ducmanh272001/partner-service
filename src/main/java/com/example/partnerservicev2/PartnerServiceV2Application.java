package com.example.partnerservicev2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PartnerServiceV2Application {

	public static void main(String[] args) {
		SpringApplication.run(PartnerServiceV2Application.class, args);
	}

}
