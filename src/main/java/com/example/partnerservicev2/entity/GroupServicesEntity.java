package com.example.partnerservicev2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.time.OffsetDateTime;

@Entity
@Table(name = "group_services")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupServicesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private OffsetDateTime create_at;
    private BigInteger create_by;
    private OffsetDateTime update_at;
    private BigInteger update_by;
    private OffsetDateTime delete_at;
    private BigInteger delete_by;
    private String group_service_code;
    private String group_service_name;
    private String status;
    private String description;
}
