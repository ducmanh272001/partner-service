package com.example.partnerservicev2.controller;

import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesRequest;
import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.GetGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.UpdateGroupServicesRequest;
import com.example.partnerservicev2.service.groupservices.GroupServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/group-services")
public class GroupServicesController {
    @Autowired
    private GroupServicesService service;

    @GetMapping("/list")
    public List<GetGroupServicesResponse> getAllGroupServices() {
        return service.getAllGroupServices();
    }

    @PostMapping("/")
    public CreateGroupServicesResponse createGroupServices(@RequestBody CreateGroupServicesRequest request) {
        return service.createGroupServices(request);
    }

    @GetMapping("/{id}")
    public GetGroupServicesResponse getGroupServices(@PathVariable(name = "id") Long id) {
        return service.getGroupServices(id);
    }

    @PutMapping("/{id}")
    public void updateGroupServices(@PathVariable(name = "id") Long id, @RequestBody UpdateGroupServicesRequest request) {
        service.updateGroupServices(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteGroupServices(@PathVariable(name = "id") Long id) {
        service.deleteGroupServices(id);
    }
}
