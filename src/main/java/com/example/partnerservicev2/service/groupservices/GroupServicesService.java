package com.example.partnerservicev2.service.groupservices;

import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesRequest;
import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.GetGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.UpdateGroupServicesRequest;

import java.util.List;

public interface GroupServicesService {
    List<GetGroupServicesResponse> getAllGroupServices();
    CreateGroupServicesResponse createGroupServices(CreateGroupServicesRequest request);
    GetGroupServicesResponse getGroupServices(Long id);
    void updateGroupServices(Long id, UpdateGroupServicesRequest request);
    void deleteGroupServices(Long id);
}
