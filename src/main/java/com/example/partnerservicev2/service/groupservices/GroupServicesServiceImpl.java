package com.example.partnerservicev2.service.groupservices;

import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesRequest;
import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.GetGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.UpdateGroupServicesRequest;
import com.example.partnerservicev2.entity.GroupServicesEntity;
import com.example.partnerservicev2.mapper.GroupServicesMapper;
import com.example.partnerservicev2.repository.GroupServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupServicesServiceImpl implements GroupServicesService {
    @Autowired
    private GroupServicesRepository groupServicesRepository;
    @Autowired
    private GroupServicesMapper groupServicesMapper;

    @Override
    public List<GetGroupServicesResponse> getAllGroupServices() {
        List<GroupServicesEntity> list = groupServicesRepository.findAll();
        return list.stream().map(groupServicesMapper::mapToGetGroupServicesResponse).collect(Collectors.toList());
    }

    @Override
    public CreateGroupServicesResponse createGroupServices(CreateGroupServicesRequest request) {
        GroupServicesEntity groupServices = groupServicesMapper.mapToGroupServicesEntity(request);
        groupServicesRepository.save(groupServices);
        return groupServicesMapper.mapToCreateGroupServicesResponse(groupServices);
    }

    @Override
    public GetGroupServicesResponse getGroupServices(Long id) {
        GroupServicesEntity groupServices = groupServicesRepository.findById(id).orElse(null);
        return groupServicesMapper.mapToGetGroupServicesResponse(groupServices);
    }

    @Override
    public void updateGroupServices(Long id, UpdateGroupServicesRequest request) {
        GroupServicesEntity groupServices = groupServicesRepository.findById(id).orElse(null);
        groupServicesMapper.updateGroupServices(groupServices, request);
        groupServicesRepository.save(groupServices);
    }

    @Override
    public void deleteGroupServices(Long id) {
        GroupServicesEntity groupServices = groupServicesRepository.findById(id).orElse(null);
        groupServicesRepository.delete(groupServices);
    }
}
