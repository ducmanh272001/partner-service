package com.example.partnerservicev2.mapper;

import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesRequest;
import com.example.partnerservicev2.dto.groupservices.CreateGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.GetGroupServicesResponse;
import com.example.partnerservicev2.dto.groupservices.UpdateGroupServicesRequest;
import com.example.partnerservicev2.entity.GroupServicesEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
        componentModel = "spring"
)
public interface GroupServicesMapper {
    GetGroupServicesResponse mapToGetGroupServicesResponse(GroupServicesEntity groupServices);

    GroupServicesEntity mapToGroupServicesEntity(CreateGroupServicesRequest request);

    CreateGroupServicesResponse mapToCreateGroupServicesResponse(GroupServicesEntity groupServices);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateGroupServices(@MappingTarget GroupServicesEntity groupServices, UpdateGroupServicesRequest request);
}
